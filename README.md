# PSONO Client - Password Manager


# Canonical source

The canonical source of PSONO Client is [hosted on GitLab.com](https://gitlab.com/psono/psono-client).


# pour mettre à jour
[suivre ce tuto](https://garrytrinder.github.io/2020/03/keeping-your-fork-up-to-date)
Puis :

- résoudre les conflits (changements par rapport à la version originale pour pouvoir host sur kubernetees)
- changer src/common/data/VERSION.txt pour que la console admin affiche la bonne version
- lancer le build sur OKD
