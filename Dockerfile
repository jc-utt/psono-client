FROM node:16

# LABEL maintainer="Sascha Pfeiffer <sascha.pfeiffer@psono.com>"

WORKDIR /app

COPY package.json .
COPY package-lock.json .

RUN npm i

COPY . .

RUN chmod 777 var/prep-build.sh

RUN var/prep-build.sh

RUN npm run buildwebclient

EXPOSE 8080

CMD [ "node" , "customServer.js"]


# COPY ./configs/nginx.conf /etc/nginx/nginx.conf
# COPY ./configs/default.conf /etc/nginx/conf.d/default.conf
# COPY ./configs/cmd.sh /root/cmd.sh
# COPY ./build/webclient /usr/share/nginx/html/
